import styled from '@emotion/styled';

/* eslint-disable-next-line */
export interface SharedProps {
  name: string
}

const StyledShared = styled.div`
  color: pink;
`;

export function Shared(props: SharedProps) {
  return (
    <StyledShared>
      <h1>{`Welcome to Shared! I have changed ${props.name}`}</h1>
    </StyledShared>
  );
}

export default Shared;
