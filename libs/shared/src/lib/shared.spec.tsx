import { render } from '@testing-library/react';

import Shared from './shared';

describe('Shared', () => {
  const Tname = 'Test';
  it('should render successfully', () => {
    const { baseElement } = render(<Shared name={Tname} />);
    expect(baseElement).toBeTruthy();
  });
});
