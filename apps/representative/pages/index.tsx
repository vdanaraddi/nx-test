import styled from '@emotion/styled';
import { Shared } from '@imprint-engine/shared';

const StyledPage = styled.div`
  .page {
  }
`;

export function Index() {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.@emotion/styled file.
   */
  return (
    <StyledPage>
      <div>
        <Shared name='Representative'/>
      </div>
    </StyledPage>
  );
}

export default Index;
