import styled from '@emotion/styled';
import { Shared } from '@imprint-engine/shared';
import { Testlib } from '@imprint-engine/testlib';

const StyledPage = styled.div`
  .page {
  }
`;

export function Index() {
  /*
   * Replace the elements below with your own.
   *
   * Note: The corresponding styles are in the ./index.@emotion/styled file.
   */
  return (
    <StyledPage>
      <Shared name='Customer'/>
      <Testlib />
    </StyledPage>
  );
}

export default Index;
